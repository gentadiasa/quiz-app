import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, SafeAreaView } from 'react-native';
import TEMP_QUESTIONS from '../data/computers';
import {Button, ButtonContainer} from '../components/button';
import { Alert } from '../components/alert';
 
class Quiz extends Component{
    state = {
        correctCount:0,
        totalCount: TEMP_QUESTIONS.length,
        activeQuestionIndex: 0,
        answered: false,
        answerCorrect: false,
    };

    answer = (correct) => {
        this.setState(state =>{
            let nextState = { answered: true};

            if (correct){
                nextState.correctCount = state.correctCount+1;
                nextState.answerCorrect = true;
            } else {
                nextState.answerCorrect = false;
            }
            return nextState;
        },
        () => {
            this.nextQuestion();
            }
        );
    }; 

    nextQuestion = () => {
        this.setState(state =>{
            let nextIndex = state.activeQuestionIndex +1;

            if (nextIndex >= state.totalCount){
                nextIndex = 0;
            }

            return{
                activeQuestionIndex: nextIndex
            };
        });
    };

    render(){
        const question = TEMP_QUESTIONS[this.state.activeQuestionIndex];
        return (
            <View style={styles.container}>
                <StatusBar barStyle="Light-content"/>
                <SafeAreaView style={styles.safearea}>
                    <View>
                        <Text style={styles.text}>{question.question}</Text>
                            <ButtonContainer>
                            {question.answers.map(answer => (
                                <Button
                                key={answer.id}
                                text={answer.text}
                                onPress={() => this.answer(answer.correct)} />
                            ))}
                        </ButtonContainer>
                    </View>
                    
                    <Text style={styles.text}>
                    {`${this.state.correctCount}/${
                    this.state.totalCount}`}
                    </Text>
                </SafeAreaView>
                <Alert correct={this.state.answerCorrect} visible={this.state.answered} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor:"#00E0FF",
        flex:1,
        paddingHorizontal:20,
    },
    text:{
        color: '#fff',
        fontSize: 20,
        textAlign: "center",
        letterSpacing: -0.02,
        fontWeight: "600"
    },
    safearea:{
        flex:1,
        marginTop:100,
        justifyContent:'space-between'
    },

});

export default Quiz;